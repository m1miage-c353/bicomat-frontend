import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { AuthenticationRequest } from 'src/app/shared/services/models/authentication-request';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  public newUser = false;
  public loginForm: FormGroup;
  public Afficher: boolean = false
  //public errorMessage: any;
  errorMessages: Array<String> = [];
  request: AuthenticationRequest = {};

  constructor(private fb: FormBuilder, public router: Router,  private authService: AuthenticationService) {
     
    this.loginForm = this.fb.group({
      email: ["", [Validators.required]],
      password: ["", Validators.required],
      
    });
  }

  ngOnInit() {
    
  }

  login() {
    this.request.login = this.loginForm.value["email"];
    this.request.password = this.loginForm.value["password"];
    
    // debugger;
    this.authService.authentication(this.request)
    .subscribe({
      next: async (data)=>{
        sessionStorage.setItem('fullName', this.request.login as string);
        
        // console.log('Authentication successful:', data);
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(data.token as string);
        
        await this.router.navigate(['/dashboard/default']);
    
      },
      error: (err)=>{
        // console.log(err);
        this.errorMessages.push("Login ou mot de passe incorecte !");
      }
    })


    // if (this.loginForm.value["email"] == "Test@gmail.com" && this.loginForm.value["password"] == "test123") {
    //   let user = {
    //     email: "Test@gmail.com",
    //     password: "test123",
    //     name: "test user",
    //   };
    //   localStorage.setItem("user", JSON.stringify(user));
    //   this.router.navigate(["/dashboard/default"]);
    // }
  }

  showPassword(){
    this.Afficher = !this.Afficher
  }
}
