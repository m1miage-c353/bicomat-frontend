import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper.component.html',
  styleUrls: ['./swiper.component.scss']
})
export class SwiperComponent implements OnInit{
   connectedUser: String = "";

  constructor(
    private authservice: AuthenticationService
  ) { }

  ngOnInit(): void {
    this.connectedUser = this.authservice.getUserConnected();
  }
  
  // public username = localStorage.getItem("username");
  
}
