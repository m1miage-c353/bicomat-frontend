import { Injectable } from '@angular/core';
import { baseApiConfig } from '../../../api-configuration';
import { AuthenticationRequest } from './models/authentication-request';
import { Observable, tap } from 'rxjs';
import { AuthenticationResponse } from './models/authentication-response';
import { HttpClient} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authUrl: string = baseApiConfig.hostcore 
  private accessToken: string = '';

  constructor(private httpClient: HttpClient, private cookieService: CookieService) { }

  getUserConnected(): String{
    if(sessionStorage.getItem('fullName')){
      return sessionStorage.getItem('fullName') as string;
    }
    return "";
  }

  authentication(request: AuthenticationRequest): Observable<AuthenticationResponse>{
    // debugger;
    return this.httpClient.post<AuthenticationResponse>(this.authUrl+'core-metier/auth/authenticate',request)
    .pipe(
      // Utilisez tap pour stocker le token dans les cookies après la réussite de l'authentification
      tap(data => {
        if (data && data.token) {
          this.cookieService.set('accessToken', data.token);
        }
      })
    );
  }
}
