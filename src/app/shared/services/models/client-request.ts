import { CompteRequest } from '../models/compte-request';
import { CartebancaireRequest } from '../models/carte-bancaire-request';
export interface ClientRequest {
    id?: number;
    nom?: string;
    prenom?: string;
    email?: string;
    typeClient?: string;
    compteRequest?: CompteRequest;
    carteBancaireRequest?: CartebancaireRequest;
}