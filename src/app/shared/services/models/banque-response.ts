export interface BanqueResponse {
    id?: number;
    nomBanque?: string;
    adresse?: string;
}