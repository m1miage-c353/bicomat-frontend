export interface ClientInterneRequest {
    id?: number;
    anneeArrivee?: string;
    numeroContrat?: string;
    agency?: number;
    numeroPortable?: number;
    idClient?: string;
}