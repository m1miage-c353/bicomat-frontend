export interface BanqueRequest {
    id?: number;
    nomBanque?: string;
    adresse?: string;
}