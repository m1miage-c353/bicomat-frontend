export interface CartebancaireRequest {
    typeCarte?: string;
    echeance?: string;
    codeCrypto?: string;
}