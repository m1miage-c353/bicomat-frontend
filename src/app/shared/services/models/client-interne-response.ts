export interface ClientInterneResponse {
    id?: number;
    anneeArrivee?: string;
    numeroContrat?: string;
    agency?: number;
    numeroPortable?: number;
    idClient?: string;
    status?: boolean;
}