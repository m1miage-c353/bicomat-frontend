export interface ClientResponse {
    id?: number;
    status?: string;
    nom?: string;
    prenom?: string;
    typeClient?: string;
    email?: string;
}