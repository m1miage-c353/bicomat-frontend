import { Injectable } from '@angular/core';
import { baseApiConfig } from '../../../api-configuration';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { RolesRequest } from './models/role-request';
import { RolesResponse } from './models/role-response';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  apiUrl:string = baseApiConfig.hostcore +'core-metier/roles';

  constructor(private httpClient: HttpClient, private cookieService: CookieService) { }
debbuger;
  getRoles(): Observable<any> {
    const token = this.cookieService.get('accessToken');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.httpClient.get<any>(this.apiUrl+'/list', { headers })
    .pipe(
      map((response) => response)
    );   
  }


  createRole(roleData:RolesRequest): Observable<RolesResponse>{
      const token = this.cookieService.get('accessToken');
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.httpClient.post<RolesResponse>(this.apiUrl+'/save',roleData, { headers });
  }

}
