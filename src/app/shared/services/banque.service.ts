import { Injectable } from '@angular/core';
import { baseApiConfig } from '../../../api-configuration';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { BanqueRequest } from './models/banque-request';
import { BanqueResponse } from './models/banque-response';

@Injectable({
  providedIn: 'root'
})
export class BanqueService {
  apiUrl:string = baseApiConfig.hostcore +'core-metier/banques';

  constructor(private httpClient: HttpClient, private cookieService: CookieService) { }

  getBanques(): Observable<any> {
    const token = this.cookieService.get('accessToken');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.httpClient.get<any>(this.apiUrl+'/list', { headers })
    .pipe(
      map((response) => response)
    );   
  }
}
