import { Injectable } from '@angular/core';
import { baseApiConfig } from '../../../api-configuration';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { ClientInterneRequest } from './models/client-interne-request';
import { ClientInterneResponse } from './models/client-interne-response';


@Injectable({
  providedIn: 'root'
})
export class ClientInterneService {
  apiUrlList:string = baseApiConfig.hostcore +'core-metier/clientinternes';
  apiUrlCreate:string = baseApiConfig.hostmetier +'ebank/clientinternes';

  constructor(private httpClient: HttpClient, private cookieService: CookieService) { }

  getClientinternes(): Observable<any> {
    const token = this.cookieService.get('accessToken');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.httpClient.get<any>(this.apiUrlList+'/list', { headers })
    .pipe(
      map((response) => response)
    );   
  }


  createClient(clientData:ClientInterneRequest): Observable<ClientInterneResponse>{
      const token = this.cookieService.get('accessToken');
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.httpClient.post<ClientInterneResponse>(this.apiUrlCreate+'/creer',clientData, { headers });
  }

}
