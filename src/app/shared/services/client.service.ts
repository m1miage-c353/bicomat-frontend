import { Injectable } from '@angular/core';
import { baseApiConfig } from '../../../api-configuration';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { ClientRequest } from './models/client-request';
import { ClientResponse } from './models/client-response';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  apiUrlList:string = baseApiConfig.hostcore +'core-metier/clients';
  apiUrlView:string = baseApiConfig.hostcore +'core-metier/clientinternes/view';
  apiUrlCreate:string = baseApiConfig.hostmetier +'ebank/clients';

  constructor(private httpClient: HttpClient, private cookieService: CookieService) { }

  getClients(): Observable<any> {
    const token = this.cookieService.get('accessToken');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.httpClient.get<any>(this.apiUrlList+'/list', { headers })
    .pipe(
      map((response) => response)
    );   
  }


  createClient(clientData:ClientRequest): Observable<ClientResponse>{
      const token = this.cookieService.get('accessToken');
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.httpClient.post<ClientResponse>(this.apiUrlCreate+'/creer',clientData, { headers });
  }

  getClientById(id: string): Observable<any> {
    const token = this.cookieService.get('accessToken');
    const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);
    return this.httpClient.get<any>(`${this.apiUrlView}/${id}`, { headers })
    .pipe(
      map((response) => response)
    );  
  }

}
