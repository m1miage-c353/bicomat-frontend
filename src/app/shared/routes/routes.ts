import { Routes } from "@angular/router";

export const content: Routes = [
  {
    path: "dashboard",
    loadChildren: () => import("../../components/dashboard/dashboard.module").then((m) => m.DashboardModule),
  },
  {
    path: "parametrages",
    loadChildren: () => import("../../components/parametrages/parametrages.module").then((m) => m.ParametragesModule),
  },
];
