import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { Observable } from "rxjs";
@Injectable({
  providedIn: "root",
})
export class AdminGuard  {
  constructor(public router: Router, private cookieService: CookieService) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // Guard for user is login or not
    let token = this.cookieService.get('accessToken');;
    let username = sessionStorage.getItem("fullName");
    if (!token || token === null) {
      this.router.navigate(["/auth/login"]);
      return true;
    } else if (token) {
      if (!Object.keys(token).length) {
        this.router.navigate(["/auth/login"]);
        return true;
      }
    }
    return true;
  }
}
