import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ClientsComponent } from "./clients/clients.component";
import { ClientsInternesComponent } from "./clients-internes/clients-internes.component";
import { ConseillersComponent } from "./conseillers/conseillers.component";

import { ActivationsComponent } from "./activations/activations.component";
import { RolesComponent } from "./roles/roles.component";




const routes: Routes = [
  {
    path: "",
    children: [
      {
        path: "activations",
        component: ActivationsComponent,
      },
      {
        path: "clients",
        component: ClientsComponent,
      },
      {
        path: "clients-internes",
        component: ClientsInternesComponent,
      },
      {
        path: "conseillers",
        component: ConseillersComponent,
      },
      {
        path: "roles",
        component: RolesComponent,
      },



    
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParametragesRoutingModule {}
