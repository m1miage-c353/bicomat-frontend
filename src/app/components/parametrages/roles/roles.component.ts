import { Component, Input, OnInit, QueryList, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgbdSortableHeader, SortEvent } from 'src/app/shared/directives/NgbdSortableHeader';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RolesResponse } from 'src/app/shared/services/models/role-response';
import { RoleService } from 'src/app/shared/services/role.service';
import { RolesRequest } from 'src/app/shared/services/models/role-request';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrl: './roles.component.scss',
  encapsulation: ViewEncapsulation.None,
  providers: [NgbModalConfig, NgbModal]
})
export class RolesComponent implements OnInit{

  closeResult: string;
  submitted = false;
  roleForm: FormGroup;
  roleEditForm: FormGroup;
  listRoles: RolesResponse[] = [];
  request: RolesRequest = {};


  constructor(private fb: FormBuilder,private roleService: RoleService, config: NgbModalConfig, public modalService: NgbModal) {

    config.backdrop = 'static';
    config.keyboard = false;

  }

 
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  openModal(content) {
    this.modalService.open(content, { centered: true });
  }



  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
  }

  ngOnInit() {
    this.loadRoles();
    this.roleForm = this.fb.group({
      libelle: ["", Validators.required],
    });
    this.roleForm = this.fb.group({
      id:"",
      libelle: ["", Validators.required],
    });
  }

  loadRoles() {
    this.roleService.getRoles()
    .subscribe({
      next: (res) => {
        this.listRoles = res;
      },
      error: (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Échec',
          text: 'Erreur lors de la recherche des rôles:'+ error,
        });
        console.error('', error);
      }
    })

  }

  createRole() {
    if (this.roleForm.valid) {
      this.request.libelle = this.roleForm.value["libelle"];

      this.roleService.createRole(this.request)
      .subscribe({
        next: (res) => {
          this.loadRoles();
          this.roleForm.reset();
          Swal.fire({
            icon: 'success',
            title: 'Succès',
            text: 'Role créé avec succès !',
          });
        },
        error: (error) => {
          Swal.fire({
            icon: 'error',
            title: 'Échec',
            text: 'Une erreur s\'est produite lors de de la création.'+ error,
          });
        }
      })

    } else {
      Swal.fire({
        icon: 'error',
        title: 'Échec',
        text: 'Le formulaire n\'est pas valide.',
      });
    }
    
  }


}
