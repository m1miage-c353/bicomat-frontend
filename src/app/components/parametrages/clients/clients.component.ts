import { Component, Input, OnInit, QueryList, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgbdSortableHeader, SortEvent } from 'src/app/shared/directives/NgbdSortableHeader';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ClientService } from 'src/app/shared/services/client.service';
import { ClientRequest } from 'src/app/shared/services/models/client-request';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientResponse } from 'src/app/shared/services/models/client-response';
import { BanqueService } from 'src/app/shared/services/banque.service';
import { BanqueResponse } from 'src/app/shared/services/models/banque-response';
import { CompteRequest } from 'src/app/shared/services/models/compte-request';
import { CartebancaireRequest } from 'src/app/shared/services/models/carte-bancaire-request';
const Swal = require('sweetalert2');

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrl: './clients.component.scss',
  encapsulation: ViewEncapsulation.None,
  providers: [NgbModalConfig, NgbModal]
})
export class ClientsComponent implements OnInit{

  closeResult: string;
  clientForm: FormGroup;
  listClients: ClientResponse[] = [];
  listBanques: BanqueResponse[] = [];

  constructor(private fb: FormBuilder,private clientService: ClientService, config: NgbModalConfig, public modalService: NgbModal, private banqueService: BanqueService) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  openModal(content) {
    this.modalService.open(content, { centered: true });
  }


  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
  }

  ngOnInit() {
    this.loadClients();
    this.loadBanques();
    this.clientForm = this.fb.group({
      nom: ["", Validators.required],
      prenom: ["", Validators.required],
      email: ["", Validators.required],
      codecrypto: ["", Validators.required],
      typeclient: ["", Validators.required],
      echeance: ["", Validators.required],
      typecompte: [""],
      typecarte: [""],
      idBanque: [""]
    });

  }

  loadClients() {
    this.clientService.getClients()
    .subscribe({
      next: (res) => {
        // console.log(res);
        this.listClients = res;
      },
      error: (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Échec',
          text: 'Erreur lors de la recherche des clients:'+ error,
        });
      }
    })

  }
  loadBanques() {
    this.banqueService.getBanques()
    .subscribe({
      next: (res) => {
        // console.log(res);
        this.listBanques = res;
        
      },
      error: (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Échec',
          text: 'Erreur lors de la recherche des banques:'+ error,
        });
      }
    })

  }

  createClient() {
    if (this.clientForm.valid) {

      const compteReq: CompteRequest ={
        typeCompte: this.clientForm.value.typeCompte,
        banqueId: this.clientForm.value.idBanque,
      };

      const carteReq: CartebancaireRequest ={
        typeCarte: this.clientForm.value.typeCarte,
        codeCrypto: this.clientForm.value.codeCrypto,
        echeance: this.clientForm.value.echeance,
      };

      const request: ClientRequest = {
        nom: this.clientForm.value.nom,
        prenom: this.clientForm.value.prenom,
        email: this.clientForm.value.email,
        typeClient: this.clientForm.value.typeclient,
        compteRequest: compteReq,
        carteBancaireRequest: carteReq
      };

      this.clientService.createClient(request)
      .subscribe({
        next: (res) => {
          // console.log(res);
          this.loadClients();
          this.clientForm.reset();
          Swal.fire({
            icon: 'success',
            title: 'Succès',
            text: 'client créée avec succès !',
          });
        },
        error: (error) => {
          Swal.fire({
            icon: 'error',
            title: 'Échec',
            text: 'Une erreur s\'est produite lors de la création.'+ error,
          });
        }
      })

    } else {
      Swal.fire({
        icon: 'error',
        title: 'Échec',
        text: 'Le formulaire n\'est pas valide.',
      });
    }
    
  }

}
