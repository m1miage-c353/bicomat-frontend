import { Component, Input, OnInit, QueryList, ViewChildren, ViewEncapsulation } from '@angular/core';
import { NgbdSortableHeader, SortEvent } from 'src/app/shared/directives/NgbdSortableHeader';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ClientService } from 'src/app/shared/services/client.service';
import { ClientInterneRequest } from 'src/app/shared/services/models/client-interne-request';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ClientInterneService } from 'src/app/shared/services/client-interne.service';
import { ClientInterneResponse } from 'src/app/shared/services/models/client-interne-response';
import { ClientResponse } from 'src/app/shared/services/models/client-response';
import { Observable } from 'rxjs';

const Swal = require('sweetalert2');

@Component({
  selector: 'app-clients-internes',
  templateUrl: './clients-internes.component.html',
  styleUrl: './clients-internes.component.scss',
  encapsulation: ViewEncapsulation.None,
  providers: [NgbModalConfig, NgbModal]
})
export class ClientsInternesComponent implements OnInit{
  closeResult: string;
  clientForm: FormGroup;
  listClientinternes: ClientInterneResponse[] = [];
  listClients: ClientResponse[] = [];
  client: ClientResponse;

  constructor(private fb: FormBuilder,private clientService: ClientService, config: NgbModalConfig, public modalService: NgbModal, private clientinterneService: ClientInterneService) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  openModal(content) {
    this.modalService.open(content, { centered: true });
  }


  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
  }

  ngOnInit() {
    this.loadClientinternes();
    this.loadClients();
    this.clientForm = this.fb.group({
      numerocontrat: ["", Validators.required],
      numeroportable: ["", Validators.required],
      anneearrivee: ["", Validators.required],
      agency: [""],
      idClient: [""]
    });

  }

  loadClientinternes() {
    this.clientinterneService.getClientinternes()
    .subscribe({
      next: (res) => {
        // console.log(res);
        this.listClientinternes = res;
      },
      error: (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Échec',
          text: 'Erreur lors de la recherche des clients internes:'+ error,
        });
      }
    })



  }
  loadClients() {
    this.clientService.getClients()
    .subscribe({
      next: (res) => {
        // console.log(res);
        this.listClients = res;
        
      },
      error: (error) => {
        Swal.fire({
          icon: 'error',
          title: 'Échec',
          text: 'Erreur lors de la recherche des clients:'+ error,
        });
      }
    })

  }

  createClientinterne() {
    if (this.clientForm.valid) {
      const request: ClientInterneRequest = {
        anneeArrivee: this.clientForm.value.anneearrivee,
        numeroContrat: this.clientForm.value.numerocontrat,
        agency: this.clientForm.value.agency,
        numeroPortable: this.clientForm.value.numeroportable,
        idClient: this.clientForm.value.idClient
      };

      this.clientinterneService.createClient(request)
      .subscribe({
        next: (res) => {
          // console.log(res);
          this.loadClientinternes();
          this.clientForm.reset();
          Swal.fire({
            icon: 'success',
            title: 'Succès',
            text: 'client interne créé avec succès !',
          });
        },
        error: (error) => {
          Swal.fire({
            icon: 'error',
            title: 'Échec',
            text: 'Une erreur s\'est produite lors de la création.'+ error,
          });
        }
      })

    } else {
      Swal.fire({
        icon: 'error',
        title: 'Échec',
        text: 'Le formulaire n\'est pas valide.',
      });
    }
    
  }

  getClientById(id: string): void{
    this.clientService.getClientById(id)
    .subscribe(data => {
      console.log(data)
      this.client = data
    }, error => console.log(error));
  }

}
