import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ParametragesRoutingModule } from './parametrages-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ActivationsComponent } from "./activations/activations.component";
import { ClientsComponent } from "./clients/clients.component";
import { ClientsInternesComponent } from "./clients-internes/clients-internes.component";;
import { ConseillersComponent } from "./conseillers/conseillers.component";
import {MatPaginatorModule} from '@angular/material/paginator';
import { NgApexchartsModule } from 'ng-apexcharts';
import { NgxPaginationModule } from 'ngx-pagination';
import { RolesComponent } from "./roles/roles.component";



@NgModule({
  declarations: [
    ActivationsComponent,
    ConseillersComponent,
    ClientsComponent,
    ClientsInternesComponent,
    RolesComponent
  ],

  imports: [
    ParametragesRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    NgbModule,
    AppRoutingModule,
    NgApexchartsModule,
    MatPaginatorModule,
    NgxPaginationModule,

  ],
  

})

export class ParametragesModule { }
