import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChartistModule } from "ng-chartist";
import { NgChartsModule } from "ng2-charts";
import { CarouselModule } from "ngx-owl-carousel-o";
import { NgApexchartsModule } from "ng-apexcharts";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "../../shared/shared.module";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { GoogleMapsModule } from "@angular/google-maps";

import { DefaultComponent } from "./default/default.component";
import { WelcomeComponent } from "./default/welcome/welcome.component";
import { StatusComponent } from "./default/status/status.component";
import { OverallBalanceComponent } from "./default/overall-balance/overall-balance.component";
import { RecentOrdersComponent } from "./default/recent-orders/recent-orders.component";
import { ActivityComponent } from "./default/activity/activity.component";
import { RecentSalesComponent } from "./default/recent-sales/recent-sales.component";
import { TimelineComponent } from "./default/timeline/timeline.component";
import { PurchaseAccountComponent } from "./default/purchase-account/purchase-account.component";
import { TotalUsersComponent } from "./default/total-users/total-users.component";
import { FollowersGrowthComponent } from "./default/followers-growth/followers-growth.component";
import { PaperNoteComponent } from "./default/paper-note/paper-note.component";
import { OrdersProfitComponent } from "./default/orders-profit/orders-profit.component";
import { OrdersComponent } from "./default/orders-profit/orders/orders.component";
import { ProductStatusChartBoxComponent } from "./default/product-status-chart-box/product-status-chart-box.component";
import { ProfitComponent } from "./default/orders-profit/profit/profit.component";
@NgModule({
  declarations: [
    DefaultComponent,
    WelcomeComponent,
    StatusComponent,
    OverallBalanceComponent,
    RecentOrdersComponent,
    ActivityComponent,
    RecentSalesComponent,
    TimelineComponent,
    PurchaseAccountComponent,
    TotalUsersComponent,
    FollowersGrowthComponent,
    PaperNoteComponent,
    OrdersProfitComponent,
    ProfitComponent,
    OrdersComponent,
    ProductStatusChartBoxComponent,
  ],
  imports: [CommonModule, ChartistModule, CarouselModule, NgChartsModule, NgApexchartsModule, SharedModule, GoogleMapsModule, NgbModule, FormsModule, DashboardRoutingModule],
  exports: [
    ProductStatusChartBoxComponent,
    OrdersComponent,
    ProfitComponent,
    TotalUsersComponent
  ]
})
export class DashboardModule {}
